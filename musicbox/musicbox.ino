#include "pitches.h"
#include "button.h"
#include "buzzer.h"

#define PIN_BUTTON_SELECT 3
#define PIN_BUTTON_OFF 5
#define PIN_BUTTON_SPEED 4
#define PIN_BUZZER 6

Button buttonMelodySelect(PIN_BUTTON_SELECT);
Button buttonOff(PIN_BUTTON_OFF);
Button buttonSpeed(PIN_BUTTON_SPEED);
Buzzer buzzer(PIN_BUZZER);

int notes[] = {NOTE_G4, NOTE_SILENCE, NOTE_G4, NOTE_SILENCE, NOTE_G4, NOTE_SILENCE, NOTE_DS4, NOTE_SILENCE, NOTE_AS4, NOTE_SILENCE, NOTE_G4, NOTE_SILENCE, NOTE_DS4, NOTE_SILENCE, NOTE_AS4, NOTE_SILENCE, NOTE_G4, NOTE_SILENCE,
               NOTE_D5, NOTE_SILENCE, NOTE_D5, NOTE_SILENCE, NOTE_D5, NOTE_SILENCE, NOTE_DS5, NOTE_SILENCE, NOTE_AS4, NOTE_SILENCE, NOTE_FS4, NOTE_SILENCE, NOTE_DS4, NOTE_SILENCE, NOTE_AS4, NOTE_SILENCE, NOTE_G4, NOTE_SILENCE,
               NOTE_G5, NOTE_SILENCE, NOTE_G4, NOTE_SILENCE, NOTE_G5, NOTE_SILENCE, NOTE_FS5, NOTE_SILENCE, NOTE_F5, NOTE_SILENCE, NOTE_E5, NOTE_SILENCE, NOTE_DS5, NOTE_SILENCE, NOTE_E5, NOTE_SILENCE,
               NOTE_GS4, NOTE_SILENCE, NOTE_CS5, NOTE_SILENCE, NOTE_GS4, NOTE_SILENCE, NOTE_C5, NOTE_SILENCE, NOTE_B4, NOTE_SILENCE, NOTE_AS4, NOTE_SILENCE, NOTE_A4, NOTE_SILENCE, NOTE_AS4, NOTE_SILENCE,
               NOTE_DS4, NOTE_SILENCE, NOTE_FS4, NOTE_SILENCE, NOTE_DS4, NOTE_SILENCE, NOTE_AS4, NOTE_SILENCE, NOTE_G4, NOTE_SILENCE, NOTE_DS4, NOTE_SILENCE, NOTE_AS4, NOTE_SILENCE, NOTE_G4, NOTE_SILENCE};
double durations[] = {8, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 2, 2, 3, 1, 3, 1, 2, 2,
                      3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 2, 2, 3, 1, 3, 1, 2, 2,
                      3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1,
                      3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1,
                      3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3};
int melodyLength = 84;

unsigned long speeds[] = {25, 50, 100, 200, 400, 800};
int currentSpeed = 2;
int speedsLength = 6;

int currentMelody = 0;

// maybe somewhere in the future we will have one more button...
// #define PIN_BUTTON_MEL_TWO 4
// Button buttonMelodyTwo(PIN_BUTTON_MEL_TWO);

// and the second melody
int notes2[] = {NOTE_C4, NOTE_SILENCE, NOTE_G4, NOTE_SILENCE};
double durations2[] = {4, 1, 4, 1};
int melodyLength2 = 4;

int melodiesLength = 2;
void setup()
{
    buzzer.setMelody(notes, durations, melodyLength);
}

void loop()
{
    buzzer.playSound();

    if (buttonOff.wasPressed())
    {
        buzzer.turnSoundOff();
    }
    
    if (buttonMelodySelect.wasPressed())
    {
        if (currentMelody == 0)
        {
          buzzer.setMelody(notes, durations, melodyLength);
          buzzer.turnSoundOn();  
        }
        if (currentMelody == 1)
        {
          buzzer.setMelody(notes2, durations2, melodyLength2);
          buzzer.turnSoundOn();
        }

        currentMelody = (currentMelody + 1)%melodiesLength;
    }

    if (buttonSpeed.wasPressed())
    {
        currentSpeed = (currentSpeed + 1)%speedsLength;
        buzzer.setNoteDuration(speeds[currentSpeed]);
    }
}
